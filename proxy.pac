function FindProxyForURL(url, host) {
  if (shExpMatch(host, "(*.system.local|system.local)"))
    return "PROXY 192.168.65.57:80";

  return "DIRECT";
}
